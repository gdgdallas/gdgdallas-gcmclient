package com.example.cloudmessaging;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {
	
	public static final String gcmappuri = "http://gdgdallas-gcmserver.appspot.com/regsave";
	public static final String ACTION_MESSAGE = "com.example.cloudmessaging.ACTION_MESSAGE";

	@Override
	protected void onError(Context arg0, String arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onMessage(Context context, Intent data) {
		Intent i = new Intent(ACTION_MESSAGE);
		i.putExtras(data.getExtras());
		Log.d("extras", data.getExtras().toString());
		sendBroadcast(i);
	}

	@Override
	protected void onRegistered(Context context, String regId) {
		try {
			sendRegId(regId);
			Log.d("onRegistered", "successful gcm save");
			Toast.makeText(context, "Successful GCM Registration", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Log.d("extras", e.getMessage());
			Toast.makeText(context, "Error Sending Registration to App Server", Toast.LENGTH_LONG).show();
		}
        Log.d("gcmregistered", regId);
	}

	private void sendRegId(String regId) throws Exception {
		HttpPost httppost = new HttpPost(gcmappuri);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("RegistrationId", regId));
        nameValuePairs.add(new BasicNameValuePair("Model", android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL));
        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        HttpClient httpclient = new DefaultHttpClient();
        httpclient.execute(httppost);
	}

	@Override
	protected void onUnregistered(Context context, String arg1) {
		Toast.makeText(context, "Device Unregistered!", Toast.LENGTH_LONG).show();
	}
}
