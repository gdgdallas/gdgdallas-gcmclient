package com.example.cloudmessaging;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;

public class MainActivity extends Activity {
	
	public static final String SENDER_ID = "938150257417";
	
	private TextView textview; 
	
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String message = intent.getStringExtra("message");
			Log.d("message", "received " + message);
			if(!TextUtils.isEmpty(message))
				textview.setText(message);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		final String regId = GCMRegistrar.getRegistrationId(this);
		if (regId.equals("")) {
			GCMRegistrar.register(this, SENDER_ID);
		} else {
			Log.v("com.example", "Already registered");
		}
		textview = (TextView) findViewById(android.R.id.text1);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		IntentFilter filter = new IntentFilter(GCMIntentService.ACTION_MESSAGE);
		registerReceiver(receiver, filter);
		Log.d("receiver", "registered");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiver);
		Log.d("receiver", "unregistered");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.menu_unregister :
			GCMRegistrar.unregister(this);
			return true; 
		default :
			return false; 
		}
	}

}
